require 'optparse'
require 'time'

op = OptionParser.new
opts = {}

op.on('-s', "--start START", "start time") do | s |
  opts[:start] = Time.strptime("#{s}+0800", '%Y-%m-%d/%H:%M%z')
end

op.on('-e', "--end END", "end time") do | e |
  opts[:start] = Time.strptime("#{e}+0800", '%Y-%m-%d/%H:%M%z')
end

op.on('-i', "--interval [1m|5m|15M|1h]") do | interval|
  opts[:interval] = interval
end

(class<<self;self;end).module_eval do
  define_method(:usage) do |msg|
    puts op.to_s
    puts "error: #{msg}" if msg
    exit 1
  end
end

begin
  op.parse!(ARGV)

  raise OptionParser::MissingArgument if opts[:interval].nil?

  if ARGV.length != 0
    usage nil
  end
rescue
  usage $!.to_s
end


