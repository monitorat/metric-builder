import os
from os.path import join

root = '/mygod/env/metric-service/var/tsd'
java_path=join(os.environ['MYGOD_ENV_ROOT'], 'jdk1.7/bin/java')
jar = join(os.environ['MYGOD_ENV_ROOT'], 'lib','metric-builder-0.1.0-standalone.jar')
